let api = "http://localhost:3000/"
let xhr = new XMLHttpRequest();
let grid = document.querySelector('._grid')
let form = document.querySelector('form')



window.onload = () => {
    axios.get(api + "goods/")
        .then(res => {
            render(res.data.body)
        })
        .catch(err => {
            console.log(err);
        })

}


// window.onload = () => {

//     axios.get(api + form.getAttribute('data-link'))
//         .then(res => {
//             render(res.data.body)
//         })
//         .catch(err => {
//             console.log(err);
//         })

// }

// form.onsubmit = (event) => {
//     let bool = false
//     event.preventDefault()
//     let obj = {}

//     let fm = new FormData(form)

//     fm.forEach((value, key) => {
//         obj[key] = value
//     })
//     console.log(obj);
//     for (let key in obj) {
//         if (obj[key].length > 1) {
//             bool = true
//         } else {
//             bool = false
//         }
//     }
//     if (bool) axios.post(api + form.getAttribute('data-link'), obj)
//         .then(res => {
//             alert('todo created!')
//             axios.get(api + form.getAttribute('data-link'))
//                 .then(res => {
//                     render(res.data.body)
//                 })
//                 .catch(err => {
//                     console.log(err);
//                 })
//         })
// }



const render = (arr) => {
    grid.innerHTML = ''
    arr.forEach((item, index) => {
        grid.innerHTML += `
        <a class="item" href="./product.html?id=${item._id}">
       
        <div class="_flex">
            <h3>${item.name}</h3>
            <p class="_num">${index + 1}</p>
        </div>
        <p class="_p">${item.madeIn} <br/> price ${item.price} </p>
        <p>${item.publisher}</p>
        <p class="delete">  
            <input type="text" value="${item._id}" hidden>
        </p>
    
        </a>
        `
    })

    let deleteBtns = document.querySelectorAll(".delete")
    deleteBtns.forEach((btn, index) => {
        btn.onclick = () => {
            axios.delete(api + btn.childNodes[1].value).then(res => {
                alert('todo deleted!')
                axios.get(api + form.getAttribute('data-link'))
                    .then(res => {
                        render(res.data.body)
                    })
                    .catch(err => {
                        console.log(err);
                    })
            })

        }
    })

}