const mongoose = require('mongoose')

// Называние схем всегда во множественном числе
// Ключи вашей схемы крайне опасно менять/удалять
const Goods = mongoose.Schema({
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Categories",
        required: true
    },
    name: {
        type: String,
        required: true
    },
    madeIn: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    price: {
        type: Number,
        required: true
    },
    publisher: {
        type: String,
        required: true
    },
    isLiked: {
        type: Boolean,
        default: false
    },
    inCart: {
        type: Boolean,
        default: false
    },
    url: {
        type: String
    }
})

module.exports = mongoose.model('Goods', Goods)