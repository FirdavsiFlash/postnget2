const mongoose = require('mongoose')

var Categories = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        default: "AYE JIZN VARAM, VECHNOOO"
    },
})


module.exports = mongoose.model('Categories', Categories)
